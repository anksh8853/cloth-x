package com.donation.cloth_x;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class Add_Items extends AppCompatActivity {
    EditText date,item,description,alternateno,pickupadd;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    String MobilePattern = "[0-9]{10}";
    FirebaseDatabase firebaseDatabase;
    Button submitd;
    DatabaseReference reference;
    ImageButton close;
    AutoCompleteTextView location;
    ProgressBar progress;


    DatePickerDialog.OnDateSetListener setListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__items);
        checkConnection();


         location=(AutoCompleteTextView) findViewById(R.id.location);
        item=findViewById(R.id.items);
        description=findViewById(R.id.des);
        alternateno=findViewById(R.id.alternatenumber);
        pickupadd=findViewById(R.id.pickupadd);
        submitd = findViewById(R.id.submitd);
        close=findViewById(R.id.close);
        progress=findViewById(R.id.progress);
        date=findViewById(R.id.date);


        Calendar calendar=Calendar.getInstance();
        final int year=calendar.get(Calendar.YEAR);
        final int month=calendar.get(Calendar.MONTH);
        final int day=calendar.get(Calendar.DAY_OF_MONTH);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();


     reference=firebaseDatabase.getReference().child(firebaseAuth.getUid()).child("donation");
     //location spinner---------------------------------------
        List<String> category=new ArrayList<>();
        category.add("Lucknow U.P.");

        ArrayAdapter<String> arrayAdapter;
        arrayAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,category);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        location.setAdapter(arrayAdapter);
//------------------------------------------------------------------------------



        submitd.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

           checkConnection();

                String loc=location.getText().toString();
                String clothtitle="Cloth X";
                String items=item.getText().toString();
                String des=description.getText().toString();
                String altno=alternateno.getText().toString();
                String pickup=pickupadd.getText().toString();
                String dates=date.getText().toString();
                String status="Pickup Pending";
                String afterstat="";

                if (loc.isEmpty()) {
                    location.setError("Please Select Location");
                    location.requestFocus();

                }else  if (items.isEmpty()) {
                    item.setError("Please Enter No. of Item");
                    item.requestFocus();

                }
                else  if (des.isEmpty()) {
                    description.setError("Please Enter Cloth Description");
                    description.requestFocus();}

                else  if (altno.isEmpty()) {
                    alternateno.setError("Please Enter Your Calling Number");
                   alternateno.requestFocus();

                }else if (!altno.matches(MobilePattern)) {
                 alternateno.setError("Please Enter Valid Phone No");
                   alternateno.requestFocus();
                }

                else  if (pickup.isEmpty()) {
               pickupadd.setError("Please Enter Pickup Address");
                  pickupadd.requestFocus();}

                else  if (dates.isEmpty()) {
                    date.setError("Please Enter Today's Date");
                    date.requestFocus();
                }
                else if (!(loc.isEmpty() && items.isEmpty() && des.isEmpty() && altno.isEmpty()  && pickup.isEmpty()&&dates.isEmpty())) {

                    location.setEnabled(false);
                    item.setEnabled(false);
                    description.setEnabled(false);
                   alternateno.setEnabled(false);
                    pickupadd.setClickable(false);
                    submitd.setEnabled(false);
                    progress.setVisibility(View.VISIBLE);

                    Other_detail detail = new Other_detail(loc, clothtitle, items, des, altno, pickup, dates, status, afterstat);

                    reference.push().setValue(detail);



                    progress.setVisibility(View.GONE);
                    Intent intent=new Intent(Add_Items.this,user_profile.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    Toast.makeText(Add_Items.this, "Item Added Successfully", Toast.LENGTH_SHORT).show();


                }
                else {
                    Toast.makeText(Add_Items.this, "Error Occured , Try Again", Toast.LENGTH_SHORT).show();

                }


            }
        });



        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Add_Items.this,user_profile.class));
            }
        });




        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog=new DatePickerDialog(
                        Add_Items.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        month=month+1;
                        String dates=day+"/"+month+"/"+year;
                        date.setText(dates);


                    }
                },year,month,day);
                 datePickerDialog.show();

            }
        });

    }




    public void checkConnection()
    {

        ConnectivityManager manager= (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=manager.getActiveNetworkInfo();

        if (networkInfo!=null)
        {
            if (networkInfo.getType()==ConnectivityManager.TYPE_WIFI) { }

            else if (networkInfo.getType()==ConnectivityManager.TYPE_MOBILE) { } } else {
            Toast.makeText(this, "Please Enable Your Internet Connection", Toast.LENGTH_LONG).show(); }

    }
}