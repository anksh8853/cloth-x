package com.donation.cloth_x;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import static com.donation.cloth_x.R.color.colorPrimary;

public class MainActivity extends AppCompatActivity  {
    EditText lemail, lpassword;
    TextView txt4,resetpass;
    Button login;
    ProgressBar progress;


    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkConnection();


        login = findViewById(R.id.login);
        txt4 = findViewById(R.id.txt4);
        lemail = findViewById(R.id.lemail);
        lpassword = findViewById(R.id.lpassword);
        progress = findViewById(R.id.progress);
        resetpass=findViewById(R.id.resetpassword);



        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();





        String text = "Don't have account? create a new account";
        SpannableString ss = new SpannableString(text);
        ss.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black)), 0, 20, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new ForegroundColorSpan(getResources().getColor(colorPrimary)), 21, 40, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txt4.setText(ss);

        txt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(MainActivity.this, create_Account.class);
                startActivity(intent);
                finish();
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkConnection();

                String email = lemail.getText().toString();
                String pwd = lpassword.getText().toString().trim();

                if (email.isEmpty()) {
                    lemail.setError("Please Enter Registered Email Id");
                    lemail.requestFocus();
                } else if (pwd.isEmpty()) {
                    lpassword.setError("Please Enter Registered Password");
                    lpassword.requestFocus();
                } else if (!(email.isEmpty() && pwd.isEmpty())) {
                    lemail.setEnabled(false);
                    lpassword.setEnabled(false);
                    txt4.setClickable(false);
                    login.setEnabled(false);
                    resetpass.setEnabled(false);
                    progress.setVisibility(View.VISIBLE);

                    firebaseAuth.signInWithEmailAndPassword(email, pwd).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful()) {
                                lemail.setEnabled(true);
                                lpassword.setEnabled(true);
                                txt4.setClickable(true);
                                login.setEnabled(true);
                                resetpass.setEnabled(true);
                                progress.setVisibility(View.GONE);
                                Toast.makeText(MainActivity.this, "Please Enter Registered Email Id/Password", Toast.LENGTH_SHORT).show();

                            } else {
                                checkEmailVerification();

                            }
                        }
                    });


                } else {
                    progress.setVisibility(View.GONE);
                    lemail.setEnabled(true);
                    lpassword.setEnabled(true);
                    txt4.setClickable(true);
                    login.setEnabled(true);
                    resetpass.setEnabled(true);
                    Toast.makeText(MainActivity.this, "Error ocurred ,Try Again", Toast.LENGTH_SHORT).show();
                }
            }
        });


        resetpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,resetpassword.class));

            }
        });
    }







    private void checkEmailVerification() {
        FirebaseUser firebaseUser=firebaseAuth.getCurrentUser();

        Boolean emailflag=firebaseUser.isEmailVerified();
        Toast.makeText(MainActivity.this, "Login Successfully", Toast.LENGTH_SHORT).show();

        Intent i=new Intent(MainActivity.this,user_profile.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();

    }


    @Override
    protected void onStart() {
        super.onStart();
        if(firebaseAuth.getCurrentUser()!=null){
            finish();
            startActivity(new Intent(MainActivity.this,user_profile.class));
        }
    }




    public void onBackPressed(){
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void checkConnection()
    {

        ConnectivityManager manager= (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=manager.getActiveNetworkInfo();

        if (networkInfo!=null)
        {
            if (networkInfo.getType()==ConnectivityManager.TYPE_WIFI) { }

            else if (networkInfo.getType()==ConnectivityManager.TYPE_MOBILE) { } } else {
            Toast.makeText(this, "Please Enable Your Internet Connection", Toast.LENGTH_LONG).show(); }

    }


}

