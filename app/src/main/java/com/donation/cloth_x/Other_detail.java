package com.donation.cloth_x;

import android.widget.EditText;

public class Other_detail {

    public String  location,clothtitle, items, description, alternate_no, pickup_add, date,status,afterstatu;


    public Other_detail() {
    }

    public Other_detail(String location, String clothtitle, String items, String description, String alternate_no, String pickup_add, String date, String status, String afterstatu) {
        this.location = location;
        this.clothtitle = clothtitle;
        this.items = items;
        this.description = description;
        this.alternate_no = alternate_no;
        this.pickup_add = pickup_add;
        this.date = date;
        this.status = status;
        this.afterstatu = afterstatu;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getClothtitle() {
        return clothtitle;
    }

    public void setClothtitle(String clothtitle) {
        this.clothtitle = clothtitle;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAlternate_no() {
        return alternate_no;
    }

    public void setAlternate_no(String alternate_no) {
        this.alternate_no = alternate_no;
    }

    public String getPickup_add() {
        return pickup_add;
    }

    public void setPickup_add(String pickup_add) {
        this.pickup_add = pickup_add;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAfterstatu() {
        return afterstatu;
    }

    public void setAfterstatu(String afterstatu) {
        this.afterstatu = afterstatu;
    }
}