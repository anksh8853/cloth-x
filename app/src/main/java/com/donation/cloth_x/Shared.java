package com.donation.cloth_x;


import android.content.Context;
import android.content.SharedPreferences;

public class Shared {
    final static String Filename="myfilename";

    public static String readSharedSetting(Context ctx, String settingName, String defaultValue) {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(Filename, Context.MODE_PRIVATE);
        return sharedPreferences.getString(settingName,defaultValue);


    }

    public static void saveSharedpreference(Context ctx, String settingName,String settingValue){
        SharedPreferences sharedPreferences = ctx.getSharedPreferences(Filename, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(settingName,settingValue);
        editor.apply();

    }



}



