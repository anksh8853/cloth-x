package com.donation.cloth_x;

public class User {


    public String cname ,cemail,cphone;

    public User() {
    }

    public User(String cname, String cphone, String cemail) {
        this.cname = cname;
        this.cphone = cphone;
        this.cemail = cemail;

    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getCphone() {
        return cphone;
    }

    public void setCphone(String cphone) {
        this.cphone = cphone;
    }

    public String getCemail() {
        return cemail;
    }

    public void setCemail(String cemail) {
        this.cemail = cemail;
    }


}
