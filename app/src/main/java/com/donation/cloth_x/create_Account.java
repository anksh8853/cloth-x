package com.donation.cloth_x;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.PatternMatcher;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;


import java.util.Objects;

import static com.donation.cloth_x.R.color.colorPrimary;

public class create_Account extends AppCompatActivity {
    TextView txt4;
    Button submit;
    EditText name, email, phone, password;
    ProgressBar progress;
    String MobilePattern = "[0-9]{10}";
    FirebaseAuth mfirebaseAuth;
    FirebaseDatabase firebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create__account);
        checkConnection();

        txt4 = findViewById(R.id.txt4);
        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        password = findViewById(R.id.password);
        submit = findViewById(R.id.createaccount);
        progress = findViewById(R.id.progress);

        mfirebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();


        String text = "Already have a account? Login";
        SpannableString ss = new SpannableString(text);
        ss.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.black)), 0, 23, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new ForegroundColorSpan(getResources().getColor(colorPrimary)), 24, 29, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txt4.setText(ss);


        txt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(create_Account.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkConnection();
                //store all text field--------------------------------------------------------------------------------------------------
                final String cname = name.getText().toString();
                final String cemail = email.getText().toString();
                final String cphone = phone.getText().toString();
                final String cpass = password.getText().toString().trim();

                //field validation----------------------------------------------------------------------------------------------------
                if (cname.isEmpty()) {
                    name.setError("Please Enter Your Name");
                    name.requestFocus();
                } else if (cemail.isEmpty()) {
                    email.setError("Please Enter Your Email Id");
                    email.requestFocus();
                } else if (!Patterns.EMAIL_ADDRESS.matcher(cemail).matches()) {
                    email.setError("Please Enter Valid Email Id");
                    email.requestFocus();
                } else if (cphone.isEmpty()) {
                    phone.setError("Please Enter Your Phone No");
                    phone.requestFocus();
                } else if (!cphone.matches(MobilePattern)) {
                    phone.setError("Please Enter Valid Phone No");
                    phone.requestFocus();
                } else if (cpass.isEmpty()) {
                    password.setError("Please Enter Your Password");
                    password.requestFocus();

                } else if (cpass.length() < 8) {
                    password.setError("Password Should be atleast 8 character");
                    password.requestFocus();
                } else if (!(cname.isEmpty() && cemail.isEmpty() && cphone.isEmpty() && cpass.isEmpty())) {
                    email.setEnabled(false);
                    password.setEnabled(false);
                    phone.setEnabled(false);
                    name.setEnabled(false);
                    txt4.setClickable(false);
                    submit.setEnabled(false);
                    progress.setVisibility(View.VISIBLE);

                    mfirebaseAuth.createUserWithEmailAndPassword(cemail,cpass)
                            .addOnCompleteListener(create_Account.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {

                                    if (task.isSuccessful()){
                                        User user=new User(cname,cphone ,cemail);
                                      String uid=task.getResult().getUser().getUid();
                                        firebaseDatabase.getReference(uid).setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                mfirebaseAuth.signOut();
                                                progress.setVisibility(View.GONE);
                                                Toast.makeText(create_Account.this, "Registration successfully", Toast.LENGTH_SHORT).show();
                                                finish();
                                                Intent i=new Intent(create_Account.this,MainActivity.class);
                                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                startActivity(i);
                                                finish();

                                            }
                                        }); }
                                    else {
                                        Toast.makeText(create_Account.this, "User Already Exist", Toast.LENGTH_SHORT).show();
                                        progress.setVisibility(View.GONE);
                                        email.setEnabled(true);
                                        password.setEnabled(true);
                                        phone.setEnabled(true);
                                        name.setEnabled(true);
                                        txt4.setClickable(true);
                                        submit.setEnabled(true);

                                    }
                                }
                            }); }
                                   else {
                                            Toast.makeText(create_Account.this, "Error Occured ,Try Again", Toast.LENGTH_SHORT).show();
                                            progress.setVisibility(View.GONE);
                                            email.setEnabled(true);
                                            password.setEnabled(true);
                                            phone.setEnabled(true);
                                            name.setEnabled(true);
                                            txt4.setClickable(true);
                                            submit.setEnabled(true);
                                    }
                }
            }); }



    public void onBackPressed(){
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void checkConnection()
    {

        ConnectivityManager manager= (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=manager.getActiveNetworkInfo();

        if (networkInfo!=null)
        {
            if (networkInfo.getType()==ConnectivityManager.TYPE_WIFI) { }

            else if (networkInfo.getType()==ConnectivityManager.TYPE_MOBILE) { } } else {
            Toast.makeText(this, "Please Enable Your Internet Connection", Toast.LENGTH_LONG).show(); }

    }

    }


