package com.donation.cloth_x;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

import java.util.Objects;

public class myadapter extends FirebaseRecyclerAdapter<Other_detail,myadapter.myviewholder> {

    public myadapter(@NonNull FirebaseRecyclerOptions<Other_detail> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull myviewholder holder, int position, @NonNull Other_detail model) {



       holder.loc.setText(model.getLocation().toUpperCase());
       holder.des.setText(model.getDescription());
       holder.dates.setText(model.getDate());
       holder.statu.setText(model.getStatus());
       holder.callnumber.setText(model.getAlternate_no());
       holder.title.setText(model.getClothtitle());
       holder.afterstatus.setText(model.getAfterstatu());


    }

    @NonNull
    @Override
    public myviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_row,parent,false);
        return new myviewholder(view);
    }

    public void deleteItem(int position)
    {
        getSnapshots().getSnapshot(position).getRef().removeValue();
    }

    class myviewholder extends RecyclerView.ViewHolder{

        TextView loc, title, des, dates,statu,callnumber,afterstatus;
        public myviewholder(@NonNull View itemView) {
            super(itemView);

            loc=(TextView) itemView.findViewById(R.id.location);
            title=(TextView)itemView.findViewById(R.id.cloth_title);
            des=(TextView)itemView.findViewById(R.id.cloth_description);
            dates=(TextView)itemView.findViewById(R.id.date);
            statu=itemView.findViewById(R.id.status);
            callnumber=itemView.findViewById(R.id.phonenumber);
            afterstatus=itemView.findViewById(R.id.afterstatus);

        }



    }
}
