package com.donation.cloth_x;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class profile extends AppCompatActivity {
    TextView logout;
    TextView logintv,nametv,phonetv,arrow1,needhelp,otherapp,rateapp,share,aboutclothx,privacy;


    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    FirebaseDatabase firebaseDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        checkConnection();

        logintv = findViewById(R.id.emailtv);
        nametv = findViewById(R.id.nametv);
        phonetv = findViewById(R.id.phonetv);
        logout = findViewById(R.id.logout);

        needhelp = findViewById(R.id.needhelp);
        otherapp = findViewById(R.id.otherapp);
        rateapp = findViewById(R.id.rateapp);
        arrow1 = findViewById(R.id.arrow1);
        share = findViewById(R.id.share);
       aboutclothx = findViewById(R.id.aboutclothx);
       privacy=findViewById(R.id.privacy);

        // logintv.setText("Welcome"+getIntent().getStringExtra("name"));

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();

        DatabaseReference databaseReference = firebaseDatabase.getReference(firebaseAuth.getUid());

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User user = snapshot.getValue(User.class);

                if (user != null) {

                    if (user.getCemail() != null) {
                        logintv.setText(user.getCemail());

                    }
                    if (user.getCname() != null) {
                        nametv.setText(user.getCname().toUpperCase());
                    }
                    if (user.getCphone() != null) {
                        phonetv.setText(user.getCphone());
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

                Toast.makeText(profile.this, error.getCode(), Toast.LENGTH_SHORT).show();

            }
        });


        // FirebaseUser user=firebaseAuth.getCurrentUser();

        /*  nametv.setText(user.getDisplayName());*/

        //  loadUserInformation();


        arrow1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(profile.this, user_profile.class));
                finish();
            }
        });


        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(profile.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();

            }
        });


        needhelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(profile.this, needhelp.class));
                finish();

            }
        });

        otherapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(profile.this, other_apps.class));

            }
        });

        rateapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.donation.cloth_x")));

                } catch (ActivityNotFoundException e) {
                    Toast.makeText(profile.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }


            }
        });


        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent share = new Intent();
                share.setAction(Intent.ACTION_SEND);
                String sharelink = "https://play.google.com/store/apps/details?id=com.donation.cloth_x";
                share.putExtra(Intent.EXTRA_TEXT, sharelink);
                share.setType("text/plain");
                startActivity(Intent.createChooser(share, "share via"));


            }
        });


aboutclothx.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        startActivity(new Intent(profile.this,aboutprofile.class));
        finish();
    }
});

privacy.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Toast.makeText(profile.this, "Website under Maintenance", Toast.LENGTH_SHORT).show();

    }
});


    }



    @Override
    protected void onStart() {
        super.onStart();
        if(firebaseAuth.getCurrentUser()==null){
            finish();
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
        }
    }



    /*private void loadUserInformation() {

        FirebaseUser user=firebaseAuth.getCurrentUser();


        if (user!=null) {

           *//* if (user.getDisplayName() != null) {
                nametv.setText(user.getDisplayName());
            }*//*

            if (user.getEmail()!= null) {
                logintv.setText(firebaseUser.getEmail());
            }


        }
    }











*/





    /*@Override
    public void onBackPressed() {
        super.onBackPressed();
        FirebaseAuth.getInstance().signOut();
        Intent intent=new Intent(profile.this,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
        finish();
    }*/

    public void checkConnection()
    {

        ConnectivityManager manager= (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=manager.getActiveNetworkInfo();

        if (networkInfo!=null)
        {
            if (networkInfo.getType()==ConnectivityManager.TYPE_WIFI) { }

            else if (networkInfo.getType()==ConnectivityManager.TYPE_MOBILE) { } } else {
            Toast.makeText(this, "Please Enable Your Internet Connection", Toast.LENGTH_LONG).show(); }

    }
}