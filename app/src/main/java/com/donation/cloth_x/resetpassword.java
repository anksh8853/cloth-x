package com.donation.cloth_x;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class resetpassword extends AppCompatActivity {

    EditText resetemail;
    Button resetpass;
    private FirebaseAuth firebaseAuth;
    ProgressBar progress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resetpassword);

        resetemail=findViewById(R.id.resetemail);
        resetpass=findViewById(R.id.resetpass);
        firebaseAuth=FirebaseAuth.getInstance();
        progress=findViewById(R.id.progress);


        resetpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String useremail=resetemail.getText().toString().trim();

                if (useremail.isEmpty()) {
                    resetemail.setError("Please Enter Your Registered Email Id");
                    resetemail.requestFocus();
                } else if (!Patterns.EMAIL_ADDRESS.matcher(useremail).matches()) {
                   resetemail.setError("Please Enter Valid Email Id");
                    resetemail.requestFocus();
                }
                else if(!useremail.isEmpty()){
                    resetemail.setEnabled(false);
                    resetpass.setEnabled(false);
                    progress.setVisibility(View.VISIBLE);
                    firebaseAuth.sendPasswordResetEmail(useremail).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful())
                            {
                                progress.setVisibility(View.GONE);
                                Toast.makeText(resetpassword.this, "Password ResetEmail Send Successfully", Toast.LENGTH_SHORT).show();
                                finish();
                                startActivity(new Intent(resetpassword.this,MainActivity.class));
                                finish();
                            }else {
                                resetemail.setEnabled(true);
                                resetpass.setEnabled(true);
                                progress.setVisibility(View.GONE);

                                Toast.makeText(resetpassword.this, "Please Enter Registered Email Id", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });

                }
            }
        });
    }
}