package com.donation.cloth_x;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

public class user_profile extends AppCompatActivity {
    private ImageView imageView1, imageView2, imageView3, imageView4;
    TextView image1, image2, image3, image4;
    ImageButton profile,about,additem,refresh;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase;
    RecyclerView recview;
    myadapter adapter;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        checkConnection();

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
       AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        imageView1 = findViewById(R.id.imageview1);
        imageView2 = findViewById(R.id.imageview2);
        imageView3 = findViewById(R.id.imageview3);
        imageView4 = findViewById(R.id.imageview4);
        image1 = findViewById(R.id.image1);
        image2 = findViewById(R.id.image2);
        image3 = findViewById(R.id.image3);
        image4 = findViewById(R.id.image4);
        profile=findViewById(R.id.profile);
        about=findViewById(R.id.about);
        additem=findViewById(R.id.additem);
        recview=findViewById(R.id.recview);
        refresh=findViewById(R.id.refresh);


        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth=FirebaseAuth.getInstance();


        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkConnection();
                Intent intent=new Intent(user_profile.this,profile.class);
                startActivity(intent);

            }
        });


        additem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkConnection();
                startActivity(new Intent(user_profile.this,Add_Items.class));
            }
        });


        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkConnection();
                startActivity(new Intent(user_profile.this,About.class));
            }
        });

        DocumentReference user = db.collection("FILES").document("images");
        user.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot doc = task.getResult();

                    StringBuilder img1 = new StringBuilder("");
                    img1.append(doc.get("image1"));
                    image1.setText(img1.toString());
                    String image1url = image1.getText().toString();
                    Picasso.get().load(image1url).into(imageView1);

                    StringBuilder img2 = new StringBuilder("");
                    img2.append(doc.get("image2"));
                    image2.setText(img2.toString());
                    String image2url = image2.getText().toString();
                    Picasso.get().load(image2url).into(imageView2);

                    StringBuilder img3 = new StringBuilder("");
                    img3.append(doc.get("image3"));
                    image3.setText(img3.toString());
                    String image3url = image3.getText().toString();
                    Picasso.get().load(image3url).into(imageView3);

                    StringBuilder img4 = new StringBuilder("");
                    img4.append(doc.get("image4"));
                    image4.setText(img4.toString());
                    String image4url = image4.getText().toString();
                    Picasso.get().load(image4url).into(imageView4);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(user_profile.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });

refresh.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        finish();
        overridePendingTransition(0, 0);
        startActivity(getIntent());
        overridePendingTransition(0, 0);
        Toast.makeText(user_profile.this, "Refresh", Toast.LENGTH_SHORT).show();
    }
});

        //recycler view---------------------------------------------------------------------------------------------------------

        recview.setLayoutManager(new LinearLayoutManager(this));

        FirebaseRecyclerOptions<Other_detail> option=new FirebaseRecyclerOptions
                .Builder<Other_detail>().setQuery(FirebaseDatabase.getInstance()
                .getReference().child(firebaseAuth.getUid()).child("donation"),Other_detail.class).build();

   adapter=new myadapter(option);

   recview.setAdapter(adapter);

   new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT) {
       @Override
       public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
           return false;
       }

       @Override
       public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
           adapter.deleteItem(viewHolder.getAdapterPosition());
           Toast.makeText(user_profile.this, "Item Deleted", Toast.LENGTH_SHORT).show();

       }
   }).attachToRecyclerView(recview);


    }




    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    public void onBackPressed(){
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }
    public void checkConnection()
    {

        ConnectivityManager manager= (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=manager.getActiveNetworkInfo();

        if (networkInfo!=null)
        {
            if (networkInfo.getType()==ConnectivityManager.TYPE_WIFI) { }

            else if (networkInfo.getType()==ConnectivityManager.TYPE_MOBILE) { } } else {
            Toast.makeText(this, "Please Enable Your Internet Connection", Toast.LENGTH_LONG).show(); }

    }


}
